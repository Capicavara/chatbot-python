﻿processador é um circuito integrado capaz de efetuar o processamento de dados.

cpu é um circuito integrado capaz de efetuar o processamento de dados.

memória é um dispositivo usado para armazenar dados.

gabinete é o compartimento que contém a maioria dos componentes de um computador.

cabo sata é um cabo que serve para transferir dados muito rapidamente.

cabo usb é um cabo de transferencia de dados muito utilizado.

monitor é a tela utilizada para visualizar o computador.

mouse é um equipamento utilizado para controlar o computador.

teclado é um equipamento utilizado para digitar texto em um computador.

Para um computador de trabalho, é indicado um AMD Ryzen 3 3300X ou Intel Core i3 8100.

Para um computador de jogos, é indicado um AMD Ryzen 7 3800, não compra Intel não.

Para um computador de desenho, é indicado um AMD Ryzen 3 3300X ou Intel Core i7 7700.

Ghz é a medida de velocidade do processador.

Frequencia do processador é a valocidade que ele trabalha.

Overclock é aumentar a performance do processador acima do limite projetado.

Hyperthreading é uma tecnologia da Intel que transforma um núcleo de processamento em dois.

Threados definem como um processador funciona, recebendo e executando instruções.

Cache do processador é a memória utilizada para armazenar temporariamenta algumas instruções.

Socket do processador é o slot de encaixe entre o processador e a placa mãe.

DDR2 é uma arquitetura de memória mais antiga.

DDR3 é uma arquitetura de memória não tão antiga, mas já existem memórias DDR4 que são superiores.

DDR4 é a mais atual arquitetura de memória.

Frequência de memória ram é a velocidade que ela trabalha.

Socket de memória ram é o encaixe que a memória usa entre ela e a placa mãe.

Swap de memória é utilizar o disco como um auxílio a memória ram.

Dual channel é utilizar duas memórias RAM.

Memória paginável é a memória armazenada no arquivo de paginação.

Fator de forma é a tecnologia que o módulo de memória foi construido.

A velocidade da memória ram melhora sua performance, tornando-a mais rápida.

Capacitores são elementos reativos que reagem à passagem de corrente através do acúmulo de cargas elétricas, ou seja, o capacitor é capaz de armazenar energia eletroestática.

Slot de ram é o encaixe da memória na placa mãe.

Slot de processador é o encaixe do processador na placa mãe.

PCI é um tipo de slot usado para conectar dispositivos como uma GPU por exemplo.

Sata é um conector na placa mãe que serve para transferencia de dados.

Dissipador é um componente que tem o objetivo de minimizar o aquecimento.

A bateria da placa mãe serve para salvar configurações dela.

Mini ATX é um padrão de placas mãe menores que as comuns.

Micro ATX é um padrão de placas mãe muito pequenas.

ATX é um padrão de placas mãe comum.

RPM significa rotações por minuto.

IDE é um padrão antigo de encaixe para discos rígidos.

Buffer do HD é uma parte onde ele armazena um "cache" para entregar os dados mais usados rapidamente.

Partição de disco é uma parte do disco que foi dividido.

Cache de HD é o buffer onde ele armazena os dados mais utilizados, criando um cache.

AHCI é um padrão técnico definido pela Intel que especifica a operação dos adaptadores de barramento de hospedeiro SATA.

SATA do HD é a entrada usada para transferencia de dados.

Armazenamento é salvar os arquivos, armazena-los.

Velocidade de leitura é a taxa de leitura de dados no disco.

Velocidade de gravação é a taxa de gravação de dados no disco.

Placa de vídeo é um processador dedicado a processar vídeo.

GPU é a mesma coisa que uma placa de vídeo.

Memória de vídeo é um tipo de memória dedicada para armazenar dados de vídeo.

Cuda cores são os núcleos de processamento de uma placa de vídeo NVIDIA.

Shader units são os núcleos de processamento de uma placa de vídeo AMD.

HDMI é um tipo de saída-entrada de audio e vídeo 2 em 1.

VGA é um tipo de saída de vídeo.

GDDR5 é uma arquitetura de memórias de vídeo.

GDDR6 é o mais novo modelo de arquitetura de memória de video.

Vulkan é uma API de computação e gráficos 3D.

Cooler é uma ventoinha que serve para refrigeração.

Water Cooler é um método de refrigeração que utiliza liquidos.

Fan é o mesmo que ventoinha, e serve para refrigeração.

RPM da fan é a velocidade que ela gira.

Temperatura de trabalho é a temperatura comum de um dispositivo eletronico.

O tamanho da fan tem influencia na sua eficiencia, no fluxo de ar que ela gera.

dBa é o ruído produzido por um equipamento.

Fluxo de ar é um método de refrigeração.

Expectativa de vida da Fan é mais ou menos 40.000Hrs.

A marca recomendada de cooler é CoolerMaster, muito boa ja usei.

Gabinete gamer é um gabinete grande, espaçoso para uma melhor refrigeração e fluxo de ar.

Jumper de power é o conector do botão que liga o computador.

Jumper de led é o conector que liga os leds.

Jumper de reset é o conector do botão que reinicia o computador.

USB é um conector para ligar dispositivos ao computador.

USB 3.0 é um conector mais rápido.

Cable management é uma técnica para ligar os cabos no computador com o objetivo de impactar menos em sua ventilação.

O tamanho ideal de um gabinete é um bem espaçoso por dentro.

Full Tower é um gabinete bem grande, para trabalhos de alta performance ou jogos.

Fonte é um equipamento eletrônico que alimenta o computador.

Watts é a unidade que mede a potência da fonte.

Capacitor ou condensador é um componente que armazena cargas elétricas num campo elétrico, acumulando um desequilíbrio interno de carga elétrica.

Fonte modular é uma fonte onde você pode remover os cabos que não estão sendo utilizados.

80 Plus White é um selo de eficiência de energia de uma fonte de eficiência alta.

80 Plus Bronze é um selo de eficiência de energia de uma fonte de eficiência muito alta.

80 Plus Gold é um selo de eficiência de energia de uma fonte de eficiência excelente.

Fonte não modular é uma fonte onde você não pode remover os cabos que não estão sendo utilizados.

A chave de voltagem da fonte serve para alterar a voltagem, que depende da voltagem da tomada utilizada.

Cabo ATX é o cabo que alimenta a placa mãe direto da fonte.

oi.

eae.

salve.

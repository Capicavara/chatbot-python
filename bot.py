import io
import nltk
nltk.download('stopwords')
import warnings
import json as js
from nltk.corpus import stopwords
warnings.filterwarnings("ignore")
import numpy as np
import random
import string 
with open("intents.json") as file:
  data = js.load(file)
f=open('nlp python answer finals.txt','r',errors = 'ignore')
m=io.open('respostas.txt',mode="r",errors = 'ignore',encoding="utf-8")

checkpoint = "./chatbot_weights.ckpt"

def tirarStop(text):
  stopwords = ['?', '!', 'e', 'a', 'um', 'uma']
  noStop = [text for k in text not in stopwords]
  return noStop

palavras = []
intencoes = []
sentencas = []
saidas = []

# pega intenção por intenção
for intent in data["intents"]:
  
  tag = intent['tag'] 

  if tag not in intencoes:
     intencoes.append(tag)

  for pattern in intent["patterns"]:
    wrds = nltk.word_tokenize(pattern, language='portuguese')
    palavras.extend(wrds)
    sentencas.append(wrds)
    saidas.append(tag)

raw=f.read()
rawone=m.read()
raw=raw.lower()# converte pra lowercase
rawone=rawone.lower()
nltk.download('punkt') 
nltk.download('wordnet') 
sent_tokens = nltk.sent_tokenize(raw)
word_tokens = nltk.word_tokenize(raw)
sent_tokensone = nltk.sent_tokenize(rawone)
word_tokensone = nltk.word_tokenize(rawone)


sent_tokens[:2]
sent_tokensone[:2]

word_tokens[:5]
word_tokensone[:5]

lemmer = nltk.stem.WordNetLemmatizer()
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

Introduce_Ans = ["My name is PyBot.","My name is PyBot you can called me pi.","Im PyBot :) ","My name is PyBot. and my nickname is pi and i am happy to solve your queries :) "]
GREETING_INPUTS = ("hello", "hi","hiii","hii","hiiii","hiiii", "greetings", "sup", "what's up","hey",)
GREETING_RESPONSES = ["hi", "hey", "hii there", "hi there", "hello", "I am glad! You are talking to me"]
Basic_Q = ("what is python ?","what is python","what is python?","what is python.")
Basic_Ans = "Python is a high-level, interpreted, interactive and object-oriented scripting programming language python is designed to be highly readable It uses English keywords frequently where as other languages use punctuation, and it has fewer syntactical constructions than other languages."
Basic_Om = ("what is module","what is module.","what is module ","what is module ?","what is module?","what is module in python","what is module in python.","what is module in python?","what is module in python ?")
Basic_AnsM = ["Consider a module to be the same as a code library.","A file containing a set of functions you want to include in your application.","A module can define functions, classes and variables. A module can also include runnable code. Grouping related code into a module makes the code easier to understand and use."]
#nosso

Proc_P = ["qual processador é melhor indicado para um computador de trabalho?","qual processador é melhor indicado para um computador de jogos?","qual processador é melhor indicado para um computador de desenhos?", "o que é ghz?", "o que é frequencia do processador?", "o que é overclock?", "o que é hyperthreading?", "o que é thread", "o que é cache do processador?", "o que é socket do processador?"]

Ram_P = ["o que é ddr2?","o que é ddr3?","o que é ddr4?", "o que é frequência de memória ram?", "o que é socket de memória ram?", "o que é swap de ram?", "o que é dual channel?", "o que é memória paginável", "o que é fator de forma?", "no que influencia a velocidade da memória?"]

Mae_P = ["o que é capacitor?","o que é slot de ram?","o que é slot de processador?", "o que é pci?", "o que é sata?", "o que é dissipador?", "para que serve a bateria da placa mae?", "o que é mini atx", "o que é micro atx?", "o que é atx?"]

HD_P = ["o que é rpm?","o que é ide?","o que é buffer do hd?", "o que é partição de disco?", "o que é cache do hd?", "o que é ahci?", "o que é sata do hd?", "o que é armazenamento", "o que é velocidade de leitura?", "o que é velocidade de gravacao?"]

SSD_P = ["o que é disco sólido?","o que é ide?","o que é buffer do ssd?", "o que é partição de disco?", "o que é cache do ssd?", "o que é ahci?", "o que é sata do ssd?", "o que é armazenamento", "o que é velocidade de leitura?", "o que é velocidade de gravacao?"]

GPU_P = ["o que é placa de vídeo?","o que é gpu?","o que é memória de video?", "o que é cuda cores?", "o que é shader units?", "o que é hdmi?", "o que é entrada vga?", "o que é gddr5", "o que é o que é gddr6", "o que é vulkan?"]

Cooler_P = ["o que é cooler?","o que é water cooler?","o que é fan?", "o que é rpm da fan?", "o que é temperatura de trabalho?", "o que é influencia o tamanho da fan?", "o que é dba?", "o que é fluxo de ar", "qual é a espectativa de vida da fan?", "qual a marca recomentada de cooler?"]

Gabinete_P = ["o que é gabinete gamer?","o que é acrílico?","o que é jumper de power?", "o que é jumper de led?", "o que é jumper de reset?", "o que é usb?", "o que é usb 3.0?", "o que é cable management?", "qual é o tamanho de um gabinete ideal?", "o que é full tower?"]

Fonte_P = ["o que é fonte?","o que é watts?","o que é capacitor?", "o que é fonte modular?", "o que é 80 plus white?", "o que é 80 plus bronze?", "o que é 80 plus gold?", "o que é fonte não modular?", "pra que serve a chave de voltagem da fonte?", "o que é cabo atx?"]

Gen_P = ["o que é Processador?","o que e cpu?","o que é memória?", "o que é gabinete","o que é cabo sata?","o que é cabo usb?", "o que é monitor?","o que é mouse?", "o que é teclado?"]

Gen_R = ["processador é um circuito integrado capaz de efetuar o processamento de dados","cpu é um circuito integrado capaz de efetuar o processamento de dados","memória é um dispositivo usado para armazenar dados", "gabinete é o compartimento que contém a maioria dos componentes de um computador","cabo sata é um cabo que serve para transferir dados muito rapidamente","cabo usb é um cabo de transferencia de dados muito utilizado", "monitor é a tela utilizada para visualizar o computador","mouse é um equipamento utilizado para controlar o computador", "teclado é um equipamento utilizado para digitar texto em um computador"]

# FILTRANDO Cumprimento
def greeting(sentence):
    """If user's input is a greeting, return a greeting response"""
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)

# FILTRANDO Basic_Q
def basic(sentence):
    for word in Gen_P:
        if sentence.lower() == word:
            return Gen_R

# FILTRANDO Basic_QM
def basicM(sentence):
    """If user's input is a greeting, return a greeting response"""
    for word in Basic_Om:
        if sentence.lower() == word:
            return random.choice(Basic_AnsM)
        
# FILTRAR
def IntroduceMe(sentence):
    return random.choice(Introduce_Ans)


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


# RESPOSTA 1
def response(user_response):
    robo_response=''
    sent_tokens.append(user_response)
    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words=stopwords.words('portuguese'))
    tfidf = TfidfVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx=vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if(req_tfidf==0):
        robo_response=robo_response+"Desculpe, não te entendi"
        return robo_response
    else:
        robo_response = robo_response+sent_tokens[idx]
        return robo_response
      
# RESPOSTA 2
def responseone(user_response):
    robo_response=''
    sent_tokensone.append(user_response)
    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words=stopwords.words('portuguese'))
    tfidf = TfidfVec.fit_transform(sent_tokensone)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx=vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if(req_tfidf==0):
        robo_response=robo_response+"Desculpa ai bixo, te entedi não"
        return robo_response
    else:
        robo_response = robo_response+sent_tokensone[idx]
        return robo_response


def chat(user_response):
    user_response=user_response.lower()
    keyword = ""
    keywordone = ""
    keywordsecond = ""
    
    if(user_response!='Tchau'):
        if(user_response=='Vlw' or user_response=='Obrigado' ):
            flag=False
            return "Obrigado vc"
        elif(basicM(user_response)!=None):
            return basicM(user_response)
        else:
            if(user_response.find(keyword) != -1 or user_response.find(keywordone) != -1 or user_response.find(keywordsecond) != -1):

                return responseone(user_response)
                sent_tokensone.remove(user_response)
            elif(greeting(user_response)!=None):
                return greeting(user_response)
            elif(user_response.find("seu nome") != -1 or user_response.find(" seu nome") != -1 or user_response.find("seu nome ") != -1 or user_response.find(" seu nome ") != -1):
                return IntroduceMe(user_response)
            elif(basic(user_response)!=None):
                return basic(user_response)
            else:
                return response(user_response)
                sent_tokens.remove(user_response)
                
    else:
        flag=False
        return "Tchau"
        
        

